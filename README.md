# What is ROBOCODE?

[Robocode](http://robocode.sourceforge.org) is a programming game, where the goal is to develop a robot battle tank to battle against other tanks in Java or .NET. The robot battles are running in real-time and on-screen.

# What is in THIS repository?

This is the place where I keep my own robots. I don't have too much time updating them so please don't judge me if the're old, heavy, or not very good. They can still beat some guys pretty well :P

Feel free to browse this code or ask me any questions.