package z.behavior;

import java.awt.geom.Point2D;
import java.util.*;

import robocode.*;
import robocode.robotinterfaces.IBasicEvents;

public abstract class RobotEnemyAwareBehavior implements IBasicEvents {

    protected final AdvancedRobot robot;

    protected ScannedRobotEvent enemy;
    
    protected Map<String, ScannedRobotEvent> allRobots = new HashMap<String, ScannedRobotEvent>();

    public RobotEnemyAwareBehavior( AdvancedRobot robot ) {
        this.robot = robot;
    }
    
    @Override
    public final void onStatus( StatusEvent event ) {
        if ( enemy != null && event.getTime() - enemy.getTime() > 5 ) {
            // haven't seen the enemy for 5 ticks...forget it!
            enemy = null;
        }
        onStatusEvent( event );
    }
    
    protected void onStatusEvent( StatusEvent event ) {
        // callback for extension
    }

    @Override
    public void onBulletHit( BulletHitEvent event ) {
    }

    @Override
    public void onBulletHitBullet( BulletHitBulletEvent event ) {
    }

    @Override
    public void onBulletMissed( BulletMissedEvent event ) {
    }

    @Override
    public void onDeath( DeathEvent event ) {
    }

    @Override
    public void onHitByBullet( HitByBulletEvent event ) {
    }

    @Override
    public void onHitRobot( HitRobotEvent event ) {
    }

    @Override
    public void onHitWall( HitWallEvent event ) {
    }

    @Override
    public final void onScannedRobot( ScannedRobotEvent event ) {
        this.allRobots.put( event.getName(), event );
        if ( robot instanceof TeamRobot ) {
            TeamRobot teamRobot = ( TeamRobot ) robot;
            if ( teamRobot.isTeammate( event.getName() ) ) {
                return;
            }
        }
        if ( event.isSentryRobot() ) {
            return;
        }
        // real enemy
        this.enemy = event;
        robotScanned();
    }
    
    /**
     * Will be invoked when robot is scanned
     */
    protected void robotScanned() {
        
    }
    
    public final Point2D.Double getMyLocation() {
        return new Point2D.Double( robot.getX(), robot.getY() );
    }

    @Override
    public void onRobotDeath( RobotDeathEvent event ) {
        if ( this.enemy != null ) {
            if ( event.getName().equals( enemy.getName() ) ) {
                this.enemy = null;
            }
        }
    }

    @Override
    public void onWin( WinEvent event ) {
    }

}