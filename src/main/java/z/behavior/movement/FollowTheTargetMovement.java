package z.behavior.movement;

import java.awt.geom.*;
import java.awt.geom.Point2D.Double;

import robocode.*;
import robocode.util.Utils;

public class FollowTheTargetMovement extends MovementBehavior {

    private Rectangle2D.Double safeField;

    public FollowTheTargetMovement( AdvancedRobot robot ) {
        super( robot );
    }
    
    @Override
    public void onStatusEvent( StatusEvent event ) {
        init();
        
        if ( enemy == null ) {
            return;
        }
        
        double distance = enemy.getDistance() - robot.getWidth();

        double enemyAngle = distance > 100 ? 
                robot.getHeadingRadians() + enemy.getBearingRadians() :
                enemy.getHeadingRadians() - robot.getHeadingRadians(); // drive parallel when close
         
        double goAngle = wallSmoothing( enemyAngle, distance );
        
        setBackAsFront( robot, goAngle, distance );
    }

    public double wallSmoothing( double angle, double distance ) {
        Double myLocation = getMyLocation();
        double safeDistance = Math.max( 120, Math.min( distance, 160 ) );
        while ( !safeField.contains( project( myLocation, angle, safeDistance ) ) ) {
            angle += 0.05;
        }
        return angle;
    }

    public static Point2D.Double project( Point2D.Double sourceLocation, double angle, double length ) {
        return new Point2D.Double(
                sourceLocation.x + Math.sin( angle ) * length, 
                sourceLocation.y + Math.cos( angle ) * length );
    }

    public void setBackAsFront( AdvancedRobot robot, double goAngle, double distance ) {
        double angle = Utils.normalRelativeAngle( goAngle - robot.getHeadingRadians() );
        if ( Math.abs( angle ) > ( Math.PI / 2 ) ) {
            if ( angle < 0 ) {
                robot.setTurnRightRadians( Math.PI + angle );
            }
            else {
                robot.setTurnLeftRadians( Math.PI - angle );
            }
            robot.setBack( distance );
        }
        else {
            if ( angle < 0 ) {
                robot.setTurnLeftRadians( -1 * angle );
            }
            else {
                robot.setTurnRightRadians( angle );
            }
            robot.setAhead( distance );
        }
    }

    private void init() {
        if ( safeField == null ) {
            double sentryBorderSize = robot.getSentryBorderSize();
            double safeX = Math.max( robot.getWidth() / 2, sentryBorderSize );
            double safeY = Math.max( robot.getHeight() / 2, sentryBorderSize );
            
            safeField = new Rectangle2D.Double(
                    safeX,
                    safeY,
                    robot.getBattleFieldWidth() - safeX,
                    robot.getBattleFieldHeight() - safeY );
        }
    }

}
