package z.behavior.movement;

import robocode.AdvancedRobot;
import z.behavior.RobotEnemyAwareBehavior;

public abstract class MovementBehavior extends RobotEnemyAwareBehavior {

    public MovementBehavior( AdvancedRobot robot ) {
        super( robot );
    }

}