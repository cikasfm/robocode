package z.behavior.movement;

import robocode.*;
import z.behavior.RobotEnemyAwareBehavior;

public class RandomStopBehavior extends RobotEnemyAwareBehavior {
    
    long lastTimeStopped = 0;

    double randomTime = 5 + 10 * Math.random();

    public RandomStopBehavior( AdvancedRobot robot ) {
        super( robot );
    }
    
    @Override
    protected void onStatusEvent( StatusEvent event ) {
        long delta = event.getTime() - lastTimeStopped;
        if ( delta > randomTime ) {
            stop( event );
        }
    }

    private void stop( StatusEvent event ) {
        robot.setAhead( -0.5 * robot.getDistanceRemaining() );
        robot.setTurnRight( -2 * robot.getTurnRemaining() );
        robot.execute();
        randomTime = 5 + 10 * Math.random();
        lastTimeStopped = event.getTime();
    }

}
