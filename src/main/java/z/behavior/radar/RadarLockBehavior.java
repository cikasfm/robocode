package z.behavior.radar;

import robocode.*;
import robocode.util.Utils;
import z.behavior.RobotEnemyAwareBehavior;

/**
 * Head On Targeting radar lock
 * 
 * @author Zilvinas Vilutis
 *
 */
public class RadarLockBehavior extends RobotEnemyAwareBehavior {
    
    public RadarLockBehavior( AdvancedRobot robot ) {
        super( robot );
    }
    
    public void onStatusEvent( StatusEvent event ) {
        if ( enemy != null ) {
            double absBearing = enemy.getBearingRadians() + robot.getHeadingRadians();

            robot.setTurnRadarRightRadians( Utils.normalRelativeAngle( absBearing - robot.getRadarHeadingRadians() ) * 2 );
        }
        else {
            robot.setTurnRadarRightRadians( Double.POSITIVE_INFINITY );
        }
    }

}
