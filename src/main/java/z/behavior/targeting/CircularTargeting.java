package z.behavior.targeting;

import java.awt.*;
import java.awt.geom.Point2D;

import robocode.*;
import robocode.robotinterfaces.IPaintEvents;
import robocode.util.Utils;

/**
 * Circular Targeting from WIKI: <a href="http://robowiki.net/wiki/Circular_Targeting">http://robowiki.net/wiki/Circular_Targeting</a>
 * 
 * @author Zilvinas Vilutis
 */
public class CircularTargeting extends TargetingBehavior implements IPaintEvents {

    private double oldEnemyHeading;
    private double power = Rules.MAX_BULLET_POWER;
    
    public CircularTargeting( AdvancedRobot robot ) {
        super( robot );
    }

    @Override
    protected void robotScanned() {
        double bulletPower = Math.min( 3.0, robot.getEnergy() );
        double myX = robot.getX();
        double myY = robot.getY();
        double absoluteBearing = robot.getHeadingRadians() + enemy.getBearingRadians();
        double enemyX = robot.getX() + enemy.getDistance() * Math.sin( absoluteBearing );
        double enemyY = robot.getY() + enemy.getDistance() * Math.cos( absoluteBearing );
        double enemyHeading = enemy.getHeadingRadians();
        double enemyHeadingChange = enemyHeading - oldEnemyHeading;
        double enemyVelocity = enemy.getVelocity();
        oldEnemyHeading = enemyHeading;

        double deltaTime = 0;
        double battleFieldHeight = robot.getBattleFieldHeight(), battleFieldWidth = robot.getBattleFieldWidth();
        double predictedX = enemyX, predictedY = enemyY;
        while ( ( ++deltaTime ) * ( 20.0 - 3.0 * bulletPower ) < Point2D.Double.distance( myX, myY, predictedX, predictedY ) ) {
            predictedX += Math.sin( enemyHeading ) * enemyVelocity;
            predictedY += Math.cos( enemyHeading ) * enemyVelocity;
            enemyHeading += enemyHeadingChange;
            if ( predictedX < 18.0
                    || predictedY < 18.0
                    || predictedX > battleFieldWidth - 18.0
                    || predictedY > battleFieldHeight - 18.0 ) {

                predictedX = Math.min( Math.max( 18.0, predictedX ),
                        battleFieldWidth - 18.0 );
                predictedY = Math.min( Math.max( 18.0, predictedY ),
                        battleFieldHeight - 18.0 );
                break;
            }
        }
        double theta = Utils.normalAbsoluteAngle( Math.atan2(
                predictedX - robot.getX(), predictedY - robot.getY() ) );

        robot.setTurnRadarRightRadians( Utils.normalRelativeAngle(
                absoluteBearing - robot.getRadarHeadingRadians() ) );
        robot.setTurnGunRightRadians( Utils.normalRelativeAngle(
                theta - robot.getGunHeadingRadians() ) );
    }
    
    @Override
    protected void onFire() {
        robot.setBulletColor( Color.red );
    }
    
    @Override
    protected double getPower() {
        return power;
    }
    
    @Override
    public void onPaint( Graphics2D g ) {
        g.setColor( Color.white );
        g.drawString( robot.getName() + " bullet power:" + power, 20, 20 );
    }
    
}
