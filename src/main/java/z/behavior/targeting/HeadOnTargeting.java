package z.behavior.targeting;

import java.awt.Color;

import robocode.AdvancedRobot;
import robocode.util.Utils;

/**
 * HOT ( Head-On-Targeting ) from WIKI:
 * http://robowiki.net/wiki/Head-On_Targeting
 * 
 * Fires white color bullets
 * 
 * @author Zilvinas Vilutis
 */
public class HeadOnTargeting extends TargetingBehavior {

    public HeadOnTargeting( AdvancedRobot robot ) {
        super( robot );
    }

    @Override
    protected void robotScanned() {
        double absoluteBearing = robot.getHeadingRadians() + enemy.getBearingRadians();
        robot.setTurnGunRightRadians( Utils.normalRelativeAngle( absoluteBearing - robot.getGunHeadingRadians() ) );
    }
    
    @Override
    protected void onFire() {
        robot.setBulletColor( Color.white );
    }
    
}
