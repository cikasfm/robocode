package z.behavior.targeting;

import java.awt.geom.Point2D;

import robocode.AdvancedRobot;
import robocode.util.Utils;

/**
 * Linear targeting from WIKI: http://robowiki.net/wiki/Linear_Targeting
 * 
 * @author Zilvinas Vilutis
 */
public class LinearTargeting extends TargetingBehavior {
    
    public LinearTargeting( AdvancedRobot robot ) {
        super( robot );
    }

    @Override
    protected void robotScanned() {
        double bulletPower = Math.min( 3.0, robot.getEnergy() );
        double myX = robot.getX();
        double myY = robot.getY();
        double absoluteBearing = robot.getHeadingRadians() + enemy.getBearingRadians();
        double enemyX = robot.getX() + enemy.getDistance() * Math.sin( absoluteBearing );
        double enemyY = robot.getY() + enemy.getDistance() * Math.cos( absoluteBearing );
        double enemyHeading = enemy.getHeadingRadians();
        double enemyVelocity = enemy.getVelocity();

        double deltaTime = 0;
        double battleFieldHeight = robot.getBattleFieldHeight(), battleFieldWidth = robot.getBattleFieldWidth();
        double predictedX = enemyX, predictedY = enemyY;
        while ( ( ++deltaTime ) * ( 20.0 - 3.0 * bulletPower ) < Point2D.Double.distance( myX, myY, predictedX, predictedY ) ) {
            predictedX += Math.sin( enemyHeading ) * enemyVelocity;
            predictedY += Math.cos( enemyHeading ) * enemyVelocity;
            if ( predictedX < 18.0
                    || predictedY < 18.0
                    || predictedX > battleFieldWidth - 18.0
                    || predictedY > battleFieldHeight - 18.0 ) {
                predictedX = Math.min( Math.max( 18.0, predictedX ),
                        battleFieldWidth - 18.0 );
                predictedY = Math.min( Math.max( 18.0, predictedY ),
                        battleFieldHeight - 18.0 );
                break;
            }
        }
        double theta = Utils.normalAbsoluteAngle( Math.atan2(
                predictedX - robot.getX(), predictedY - robot.getY() ) );

        robot.setTurnRadarRightRadians(
                Utils.normalRelativeAngle( absoluteBearing - robot.getRadarHeadingRadians() ) );
        robot.setTurnGunRightRadians( Utils.normalRelativeAngle( theta - robot.getGunHeadingRadians() ) );
    }

}
