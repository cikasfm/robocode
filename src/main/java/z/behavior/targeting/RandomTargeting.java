package z.behavior.targeting;

import robocode.*;
import robocode.util.Utils;

/**
 * http://robowiki.net/wiki/Random_Targeting
 * 
 * @author Zilvinas Vilutis
 */
public class RandomTargeting extends TargetingBehavior {

    public RandomTargeting( AdvancedRobot robot ) {
        super( robot );
    }
    
    @Override
    protected void robotScanned() {
        double targetAngle = robot.getHeadingRadians() + enemy.getBearingRadians();
        
        double bulletPower = Math.max(0.1,Math.random() * 3.0);
        double escapeAngle = Math.asin(8 / Rules.getBulletSpeed(bulletPower));
        double randomAimOffset = -escapeAngle + Math.random() * 2 * escapeAngle;
     
        double headOnTargeting = targetAngle - robot.getGunHeadingRadians();
        robot.setTurnGunRightRadians(Utils.normalRelativeAngle(headOnTargeting + randomAimOffset));
    }

}
