package z.behavior.targeting;

import robocode.*;
import z.behavior.RobotEnemyAwareBehavior;

public abstract class TargetingBehavior extends RobotEnemyAwareBehavior {

    public TargetingBehavior( AdvancedRobot robot ) {
        super( robot );
    }

    @Override
    public final void onStatusEvent( StatusEvent e ) {
        if ( enemy != null && robot.getGunTurnRemaining() < 1 && robot.getGunHeat() < 0.1 ) {
            onFire();
            robot.setFire( getPower() );
        }
    }

    protected double getPower() {
        return Rules.MAX_BULLET_POWER;
    }

    /**
     * Invoked right before firing a bullet
     */
    protected void onFire() {
    }

}