package z.robot;

import java.awt.geom.Point2D;

import robocode.*;
import z.utils.EuclidUtils;

/**
 * Same simple robot, but using euclidean coordinates system to get its
 * position, calc enemy's position & etc
 * 
 * @author Zilvinas Vilutis
 */
public abstract class AbstractZRobot extends TeamRobot {

    public enum Direction {
        north,
        south,
        east,
        west, 
        northEast( north, east ), northWest( north, west ),
        southEast( south, east ), southWest( south, west );
        
        private Direction[] combination; 
        
        private Direction() {
            
        }
        
        private Direction( Direction ... directions ) {
            combination = directions;
        }
        
        public static Direction getCombined( Direction ... directions ) {
            for ( Direction direction : values() ) {
                if ( directions.length == direction.combination.length ) {
                    int matchingCount = 0;
                    for ( Direction test : directions ) {
                        for ( Direction existing : direction.combination ) {
                            if ( test == existing ) {
                                matchingCount++;
                            }
                        }
                    }
                    if ( matchingCount == direction.combination.length ) {
                        return direction;
                    }
                }
            }
            return null;
        }
    }

    protected Point2D futureEnemyLocation;
    
    /**
     * @param heading
     * @return
     */
    public Direction getDirection( double heading ) {
        Direction vertical = null, horizontal = null;
        if ( ( heading > 270 && heading < 360 ) || ( heading > 0 && heading < 90 )  ) {
            vertical = Direction.north;
        }
        if ( heading > 90 && heading < 270 ) {
            vertical = Direction.south;
        }
        if ( heading > 0 && heading < 180 ) {
            horizontal = Direction.east;
        }
        if ( heading > 180 && heading < 360 ) {
            horizontal = Direction.west;
        }
        
        if ( horizontal != null && vertical != null ) {
            return Direction.getCombined( vertical, horizontal );
        } else if ( horizontal != null ) {
            return horizontal;
        } else {
            return vertical;
        }
    }

    public AbstractZRobot() {
        super();
    }

    public Point2D getLocation() {
        return new Point2D.Double( getX(), getY() );
    }
    
    public Point2D getEnemyLocation( ScannedRobotEvent enemy ) {
        Point2D location = getLocation();
        double distance = enemy.getDistance();
        double heading = getHeading();
        double bearing = enemy.getBearing();
        double cartesianDegrees = heading + bearing;
        double direction = EuclidUtils.toEuclidDegrees( cartesianDegrees );
        double directionInRadians = Math.toRadians( direction );
        double newX = location.getX() + distance * Math.cos( directionInRadians );
        double newY = location.getY() + distance * Math.sin( directionInRadians );
        return new Point2D.Double( newX, newY );
    }
    
    public Point2D getFutureEnemyLocation( ScannedRobotEvent enemy, double time ) {
        Point2D enemyLocation = getEnemyLocation( enemy );
        if ( time == 0 ) {
            return enemyLocation;
        }
        double direction = EuclidUtils.toEuclidDegrees( enemy.getHeading() );
        Point2D futureLocation = EuclidUtils.getFutureLocation( enemyLocation, direction, time, enemy.getVelocity() );
        
        double x = futureLocation.getX();
        double y = futureLocation.getY();
        if ( x < 18 ) {
            x = 18;
        }
        if ( x > getBattleFieldWidth() - 18 ) {
            x = getBattleFieldWidth() - 18;
        }
        if ( y < 18 ) {
            y = 18;
        }
        if ( y > getBattleFieldHeight() - 18 ) {
            y = getBattleFieldHeight() - 18;
        }
        
        return futureLocation;
    }
    
    public double getFutureEnemyAngle( ScannedRobotEvent enemy, double time ) {
        Point2D enemyLocation = getFutureEnemyLocation( enemy, time );
        this.futureEnemyLocation = enemyLocation;
        Point2D myFutureLocation = EuclidUtils.getFutureLocation( getLocation(), getDirectionDegrees(), time * getVelocity() );
        double x = enemyLocation.getX() - myFutureLocation.getX();
        double y = enemyLocation.getY() - myFutureLocation.getY();
        double atan = Math.atan2( y, x );
        double direction = Math.toDegrees( atan );
        return EuclidUtils.toCarthesianDegrees( direction );
    }
    
    public Point2D getMyFutureLocation( double time ) {
        // double turnRate = Rules.getTurnRate( getVelocity() );
        return EuclidUtils.getFutureLocation( getLocation(), getDirectionDegrees(), time, getVelocity() );
    }
    
    public double getDirectionDegrees() {
        return EuclidUtils.toEuclidDegrees( getHeading() );
    }
    
    public double getDirectionRadians() {
        return Math.toRadians( getDirectionDegrees() );
    }
    
}