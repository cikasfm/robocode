package z.robot;

import java.awt.Graphics2D;
import java.util.*;

import robocode.*;
import robocode.robotinterfaces.IPaintEvents;
import z.behavior.RobotEnemyAwareBehavior;
import z.utils.RobotStats;

/**
 * Delegates behavior to {@link #allBehaviors}
 * 
 * @author zvilutis
 */
abstract class BehaviorBasedRobot extends TeamRobot {

    protected final List<RobotEnemyAwareBehavior> allBehaviors = new ArrayList<RobotEnemyAwareBehavior>();
    protected final RobotStats robotStats = new RobotStats();

    /**
     * onScannedRobot: What to do when you see another robot
     */
    public void onScannedRobot( ScannedRobotEvent event ) {
        robotStats.add( event );
        for ( RobotEnemyAwareBehavior behavior : allBehaviors ) {
            behavior.onScannedRobot( event );
        }
    }

    @Override
    public void onWin( WinEvent event ) {
        setTurnRight( Double.POSITIVE_INFINITY );
        setAhead( Double.POSITIVE_INFINITY );
    }

    @Override
    public void onStatus( StatusEvent event ) {
        for ( RobotEnemyAwareBehavior behavior : allBehaviors ) {
            behavior.onStatus( event );
        }
    }

    @Override
    public void onRobotDeath( RobotDeathEvent event ) {
        for ( RobotEnemyAwareBehavior behavior : allBehaviors ) {
            behavior.onRobotDeath( event );
        }
    }

    @Override
    public void onHitByBullet( HitByBulletEvent event ) {
        for ( RobotEnemyAwareBehavior behavior : allBehaviors ) {
            behavior.onHitByBullet( event );
        }
    }

    @Override
    public void onBulletHit( BulletHitEvent event ) {
        for ( RobotEnemyAwareBehavior behavior : allBehaviors ) {
            behavior.onBulletHit( event );
        }
    }

    @Override
    public void onBulletMissed( BulletMissedEvent event ) {
        for ( RobotEnemyAwareBehavior behavior : allBehaviors ) {
            behavior.onBulletMissed( event );
        }
    }

    @Override
    public void onPaint( Graphics2D g ) {
        for ( RobotEnemyAwareBehavior behavior : allBehaviors ) {
            if ( behavior instanceof IPaintEvents ) {
                IPaintEvents listener = ( IPaintEvents ) behavior;
                listener.onPaint( g );
            }
        }
    }

    protected final void addBehavior( RobotEnemyAwareBehavior b ) {
        allBehaviors.add( b );
    }

}