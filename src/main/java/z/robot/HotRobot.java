package z.robot;

import java.awt.Color;

import robocode.TeamRobot;
import z.behavior.movement.WaveSurfingMovement;
import z.behavior.radar.RadarLockBehavior;
import z.behavior.targeting.CircularTargeting;

/**
 * HotRobot - a robot by Zilvinas Vilutis
 * 
 * @see TeamRobot
 */
public class HotRobot extends BehaviorBasedRobot implements Cloneable {
    
    public HotRobot() {
        addBehavior( new RadarLockBehavior( this ) );
        addBehavior( new CircularTargeting( this ) );
        addBehavior( new WaveSurfingMovement( this ) );
    }

    /**
     * run: ZRobot's default behavior
     */
    public void run() {
        setColors(
                Color.green,    // bodyColor
                Color.red,      // gunColor
                Color.green,    // radarColor
                Color.red,      // bulletColor
                Color.white     //scanArcColor
            );

        setAdjustGunForRobotTurn( true );
        setAdjustRadarForGunTurn( true );
        setAdjustRadarForRobotTurn( true );
        
        execute();
        
    }
    
}