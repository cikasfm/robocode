package z.robot;

import java.awt.Color;

import z.behavior.movement.FollowTheTargetMovement;
import z.behavior.radar.RadarLockBehavior;
import z.behavior.targeting.CircularTargeting;

public class Pusher extends BehaviorBasedRobot {
    
    public Pusher() {
        addBehavior( new RadarLockBehavior( this ) );
        addBehavior( new FollowTheTargetMovement( this ) );
        addBehavior( new CircularTargeting( this ) );
    }
    
    @Override
    public void run() {
        setColors(
            Color.black,    // bodyColor
            Color.white,    // gunColor
            Color.red,      // radarColor
            Color.white,    // bulletColor
            Color.red       //scanArcColor
        );
        
        setAdjustGunForRobotTurn( true );
        setAdjustRadarForGunTurn( true );
        setAdjustRadarForRobotTurn( true );
        
        execute();
    }
    
}
