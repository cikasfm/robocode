package z.robot;

import java.awt.Color;

import robocode.*;

//API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html

/**
 * ZRobot - a robot by (your name here)
 * 
 * @see AdvancedRobot
 */
public class ZSimpleRobot extends Robot
{
    private boolean tankNearBy = false;

    private double width = 0;

    private double height = 0;

    private double qsize = 0;

    /**
     * run: ZRobot's default behavior
     */
    public void run() {
        // Initialization of the robot should be put here
        this.width = getBattleFieldWidth();
        this.height = getBattleFieldHeight();
        this.qsize = Math.sqrt( Math.pow( width / 2, 2 ) + Math.pow( height / 2, 2 ) );

        // After trying out your robot, try uncommenting the import at the top,
        // and the next line:

        setColors( Color.blue, Color.red, Color.white, Color.red, Color.white ); 
        // body,gun,radar,bullet,scan arc
        
        // Robot main loop
        while ( true ) {
            scan();
            
            turnGunRight( 45 );
            turnGunLeft( 360 );
        }
    }

    /**
     * 1. Identify enemy position vs gun heading
     * 2. Identify enemy heading vs gun heading & velocity
     * 3. Identif
     * 
     * onScannedRobot: What to do when you see another robot
     */
    public void onScannedRobot( ScannedRobotEvent e ) {

        // 1 : Let's make sure the target tank is in my "quarter"
        checkTankLocation( e );

        if ( tankNearBy ) { 
            aimAndShoot( e );
        }
        
        setAdjustGunForRobotTurn( true );
        
        turnRight( e.getBearing() );
        
        ahead( e.getDistance() / 2 );
        
        // don't shoot from far distances
        if ( e.getVelocity() == 0 ) {
            safeFire( Rules.MAX_BULLET_POWER );
        }
    }

    private double aimAndShoot( ScannedRobotEvent e ) {
        // Replace the next line with any behavior you would like
        setAdjustGunForRobotTurn( false );

        double power = Math.max( Math.min( qsize / e.getDistance(), Rules.MAX_BULLET_POWER ), Rules.MIN_BULLET_POWER );
        // Fire if the gun is ready
        safeFire( power );
        return power;
    }

    private void safeFire( double power ) {
        if ( getGunHeat() == 0 ) {
            // power must be in range of the rules
            power = Math.min( power, Rules.MAX_BULLET_POWER );
            power = Math.max( power, Rules.MIN_BULLET_POWER );
            fire( power );
        }
    }

    private void checkTankLocation( ScannedRobotEvent e ) {
        tankNearBy = e.getDistance() < qsize;
    }

    /**
     * onHitByBullet: What to do when you're hit by a bullet
     */
    public void onHitByBullet( HitByBulletEvent e ) {
        // Replace the next line with any behavior you would like
        // back( 10 );
    }

    /**
     * onHitWall: What to do when you hit a wall
     */
    public void onHitWall( HitWallEvent e ) {
        // Replace the next line with any behavior you would like
        back( 20 );
    }
    
}