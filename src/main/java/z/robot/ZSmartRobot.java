package z.robot;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.*;
import java.util.Map.Entry;

import robocode.*;
import robocode.util.Utils;
import z.utils.EuclidUtils;

//API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html

/**
 * ZRobot - a robot by (your name here)
 * 
 * @see AdvancedRobot
 */
public class ZSmartRobot extends AbstractZRobot
{
    private boolean tankNearBy = false;

    private double width = 0;

    private double height = 0;

    private double qsize = 0;

    private byte strafeDirection = 1;
    
    private Map<String, ScannedRobotEvent> tanks = new LinkedHashMap<String, ScannedRobotEvent>();
    
    private ScannedRobotEvent enemy = null;

    /**
     * run: ZRobot's default behavior
     */
    public void run() {
        // Initialization of the robot should be put here
        this.width = getBattleFieldWidth();
        this.height = getBattleFieldHeight();
        this.qsize = Math.sqrt( Math.pow( width / 2, 2 ) + Math.pow( height / 2, 2 ) );

        // body,gun,radar,bullet,scan arc
        setColors( Color.black, Color.orange, Color.orange, Color.orange, Color.white );

        setAdjustRadarForGunTurn( true );
        setAdjustRadarForRobotTurn( true );
        setAdjustGunForRobotTurn( true );

        // Robot main loop
        while ( true ) {
            
            if ( getTime() % 20 == 0 ) {
                strafeDirection *= -1;
            }
            
            turnRadarRightRadians( Double.POSITIVE_INFINITY );
            
            scan();
        }
    }

    /**
     * 1. Identify enemy position vs gun heading
     * 2. Identify enemy heading vs gun heading & velocity
     * 3. Identif
     * 
     * onScannedRobot: What to do when you see another robot
     */
    public void onScannedRobot( ScannedRobotEvent e ) {

        double absBearing = e.getBearingRadians() + getHeadingRadians();

        setTurnRadarRightRadians( Utils.normalRelativeAngle( absBearing
                - getRadarHeadingRadians() ) * 2 );
        
        findNearestEnemy( e );
        
        if ( enemy != null ) {
            doMove( enemy );
        }

        execute();
    }
    
    @Override
    public void onStatus( StatusEvent e ) {

        double power = doAim( enemy );
        
        // fire only when close
        if ( tankNearBy ) {
            // Fire if the gun is ready
            if ( getGunTurnRemaining() < 1 && getGunHeat() < 0.1 ) {
                // power must be in range of the rules
                fire( power );
            }
        }
    }

    private void findNearestEnemy( ScannedRobotEvent e ) {
        // update tanks map
        tanks.put( e.getName(), e );
        Iterator<Entry<String, ScannedRobotEvent>> iterator = tanks.entrySet().iterator();
        while ( iterator.hasNext() ) {
            Entry<String, ScannedRobotEvent> next = iterator.next();
            if ( next.getValue().getTime() + 50 < getTime() ) {
                iterator.remove();
            }
        }
        
        double nearestDistance = Double.POSITIVE_INFINITY;
        ScannedRobotEvent nearestRobot = null;
        
        for( Map.Entry<String, ScannedRobotEvent> entry : tanks.entrySet() ) {
            ScannedRobotEvent robot = entry.getValue();
            if ( getGunCooledDistance( robot ) < nearestDistance ) {
                nearestDistance = getGunCooledDistance( robot );
                nearestRobot = robot;
            }
        }
        enemy = nearestRobot;
    }

    /**
     * Chechks the distance to the other robot once the gun has cooled down
     * 
     * @param robot
     * @return
     */
    private double getGunCooledDistance( ScannedRobotEvent robot ) {
        double timeToCool = getGunCoolingTime();
        Point2D futureEnemyLocation = getFutureEnemyLocation( robot, timeToCool );
        double deltaX = futureEnemyLocation.getX() - getX();
        double deltaY = futureEnemyLocation.getY() - getY();
        return Math.sqrt( deltaX * deltaX + deltaY * deltaY );
    }

    private void doMove( ScannedRobotEvent e ) {
        // 1 : Let's make sure the target tank is in my "quarter"
        checkTankLocation( enemy );

        if ( getGunCooledDistance( enemy ) > 150 ) {
            getCloser( enemy );
        }
        else {
            strafeAround( enemy );
        }
    }

    private void getCloser( ScannedRobotEvent e ) {
        setAdjustGunForRobotTurn( true );
        setTurnRight( e.getBearing() );
        setAhead( getGunCooledDistance( e ) / 2.5 );
        if ( e.getVelocity() == 0 ) {
            safeFire( Rules.MAX_BULLET_POWER );
        }
    }
    
    @Override
    public void ahead( double distance ) {
        super.ahead( checkDistanceToWall( distance ) );
    }

    @Override
    public void setAhead( double distance ) {
        super.setAhead( checkDistanceToWall( distance ) );
    }
    
    private double checkDistanceToWall( double distance ) {
        return Math.min( getDistanceToWall( EuclidUtils.toEuclidDegrees( getHeading() ) ), distance );
    }    
    
    @SuppressWarnings( "unused" )
    private double getDistanceToWall( double euclidDegrees ) {
        Point2D location = getLocation();
        double x = location.getX();
        double y = location.getY();
        
        double sinAlfa = Math.sin( Math.toRadians( euclidDegrees ) ),
               wallX = 0, wallY = 0;

        if ( euclidDegrees > 270 && euclidDegrees < 90 ) {
            // may hit the right border
            wallY = getBattleFieldWidth() * sinAlfa;
            if ( getBattleFieldHeight() > wallY && wallY < 0 ) {
                // hit the right border
            }
        } else {
            // may hit the left border
        }
        
        if ( euclidDegrees > 0 && euclidDegrees < 180 ) {
            // may hit the top
        } else {
            // may hit the bottom
        }
        
        return getBattleFieldWidth();
    }

    private void strafeAround( ScannedRobotEvent e ) {
        double bearing = e.getBearing();
        double degrees = bearing + 90 - ( 15 * strafeDirection );
        if ( Math.abs( degrees ) > 180 ) {
            degrees = ( degrees > 0 ) ? degrees - 360 : degrees + 360;
        }
        setTurnRight( degrees );
        setAhead( 100 * strafeDirection );
    }

    private double doAim( ScannedRobotEvent event ) {
        final double FIREPOWER = 2;
        final double ROBOT_WIDTH = 16,ROBOT_HEIGHT = 16;
        // Variables prefixed with e- refer to enemy, b- refer to bullet and r- refer to robot
        final double eAbsBearing = getHeadingRadians() + event.getBearingRadians();
        final double rX = getX(), rY = getY(),
            bV = Rules.getBulletSpeed(FIREPOWER);
        final double eX = rX + event.getDistance()*Math.sin(eAbsBearing),
            eY = rY + event.getDistance()*Math.cos(eAbsBearing),
            eV = event.getVelocity(),
            eHd = event.getHeadingRadians();
        // These constants make calculating the quadratic coefficients below easier
        final double A = (eX - rX)/bV;
        final double B = eV/bV*Math.sin(eHd);
        final double C = (eY - rY)/bV;
        final double D = eV/bV*Math.cos(eHd);
        // Quadratic coefficients: a*(1/t)^2 + b*(1/t) + c = 0
        final double a = A*A + C*C;
        final double b = 2*(A*B + C*D);
        final double c = (B*B + D*D - 1);
        final double discrim = b*b - 4*a*c;
        if (discrim >= 0) {
            // Reciprocal of quadratic formula
            final double t1 = 2*a/(-b - Math.sqrt(discrim));
            final double t2 = 2*a/(-b + Math.sqrt(discrim));
            final double t = Math.min(t1, t2) >= 0 ? Math.min(t1, t2) : Math.max(t1, t2);
            // Assume enemy stops at walls
            final double endX = limit(
                eX + eV*t*Math.sin(eHd),
                ROBOT_WIDTH/2, getBattleFieldWidth() - ROBOT_WIDTH/2);
            final double endY = limit(
                eY + eV*t*Math.cos(eHd),
                ROBOT_HEIGHT/2, getBattleFieldHeight() - ROBOT_HEIGHT/2);
            setTurnGunRightRadians(robocode.util.Utils.normalRelativeAngle(
                Math.atan2(endX - rX, endY - rY)
                - getGunHeadingRadians()));
            setFire(FIREPOWER);
        }
        
        return FIREPOWER;
    }
    
    private double limit(double value, double min, double max) {
        return Math.min(max, Math.max(min, value));
    }

    private double getGunCoolingTime() {
        return getGunHeat() / getGunCoolingRate();
    }

    private void safeFire( double power ) {
        if ( getGunHeat() == 0 ) {
            // power must be in range of the rules
            power = Math.min( power, Rules.MAX_BULLET_POWER );
            power = Math.max( power, Rules.MIN_BULLET_POWER );
            fire( power );
        }
    }

    protected double getGunTurnAngleToShoot( ScannedRobotEvent e, double time ) {
        double angle = getFutureEnemyAngle( e, time ) - getGunHeading();
        
        // don't turn the gun more than 180 degrees to one direction, but change the sign
        if ( Math.abs( angle ) > 180 ) {
            angle = ( angle > 0 ) ? angle - 360 : angle + 360;
        }
        return angle;
    }

    private void checkTankLocation( ScannedRobotEvent e ) {
        tankNearBy = getGunCooledDistance( e ) < qsize;
    }
    
    @Override
    public void onRobotDeath( RobotDeathEvent event ) {
        tanks.remove( event.getName() );
        if ( enemy.getName().equals( event.getName() ) ) {
            enemy = null;
        }
    }
    
    @Override
    public void onBulletHit( BulletHitEvent event ) {
        // TODO Auto-generated method stub
    }
    
    @Override
    public void onBulletHitBullet( BulletHitBulletEvent event ) {
        // TODO Auto-generated method stub
    }
    
    @Override
    public void onBulletMissed( BulletMissedEvent event ) {
        // TODO Auto-generated method stub
    }
    
    /**
     * onHitByBullet: What to do when you're hit by a bullet
     */
    public void onHitByBullet( HitByBulletEvent e ) {
        // Replace the next line with any behavior you would like
    }

    /**
     * onHitWall: What to do when you hit a wall
     */
    public void onHitWall( HitWallEvent e ) {
        strafeDirection *= -1;
    }
    
    @Override
    public void onHitRobot( HitRobotEvent event ) {
        strafeDirection *= -1;
    }
    
    @Override
    public void onWin( WinEvent event ) {
        setAhead( Double.POSITIVE_INFINITY );
        setTurnRight( Double.POSITIVE_INFINITY );
        setTurnGunLeft( Double.POSITIVE_INFINITY );
        execute();
    }
    
}