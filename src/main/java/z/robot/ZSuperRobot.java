package z.robot;

import java.awt.*;
import java.awt.geom.*;
import java.math.*;

import robocode.*;
import robocode.Event;
import z.behavior.radar.RadarLockBehavior;
import z.utils.*;

//API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html

/**
 * ZRobot - a robot by (your name here)
 * 
 * @see AdvancedRobot
 */
public class ZSuperRobot extends AbstractZRobot {
    
    private static final RobotStats stats = new RobotStats();
    
    private byte strafeDirection = 1;
    
    private ScannedRobotEvent enemy = null;
    
    private ScannedRobotEvent fireAt = null;

    private RobotStatus status;

    private double power;
    
    private boolean isAiming = false;
    
    private int lasthitRate = 0;

    private RadarLockBehavior radarController = new RadarLockBehavior( this );

    private Rectangle2D battleField;
    
    /**
     * run: ZRobot's default behavior
     */
    public void run() {
        // Initialization of the robot should be put here

        // body, gun, radar, bullet, scan arc
        setColors( Color.red, Color.blue, Color.yellow, Color.red, Color.black );

        setAdjustRadarForGunTurn( true );
        setAdjustRadarForRobotTurn( true );
        setAdjustGunForRobotTurn( true );
        
        addCustomEvent( new Condition("velocityChanged") {
            @Override
            public boolean test() {
                if ( stats.getPreviousStatus() != null ) {
                    return stats.getCurrentStatus().getVelocity() == stats.getPreviousStatus().getVelocity();
                }
                return false;
            }
        } );
        
        battleField = new Rectangle2D.Double( ( int ) getWidth(), ( int ) getHeight(), getBattleFieldWidth() - 2 * getWidth() , getBattleFieldHeight() - 2 * getHeight() );
        
        setTurnRadarRightRadians( Double.POSITIVE_INFINITY );

        // Robot main loop
        while ( true ) {
            
            doAim();
            
            doFire();
            
            doMove();
            
            execute();
        }
        
    }

    private void doFire() {
        
        if ( enemy == null ) {
            return;
        }
        
        if ( !isAiming || fireAt == null ) {
            return;
        }
        
        double distance = fireAt.getDistance();
        
        power = 300 / distance;
        double bulletVelcity = 20 - 3 * power;
        double bulletTime = Math.round( distance / bulletVelcity );
        double timeToTurn = BigDecimal.valueOf( Math.abs( fireAt.getBearing() / Rules.MAX_TURN_RATE ) ).setScale( 0, RoundingMode.UP ).doubleValue();
        double time = bulletTime + Math.max( timeToTurn, getGunCoolingTime() );
        
        // hit rate can be 0...5, so we allow 5% to 30% of accuracy based on previous hit rate
        if ( getGunTurnRemaining() * time < 1 ) {
            // save energy for better times
            return;
        }
        
        // Fire if the gun is ready
        if ( safeFire( power ) ) {
            isAiming = false;
            fireAt = null;
        }
    }

    @Override
    public void onStatus( StatusEvent e ) {
        
        radarController.onStatus( e );

        status = e.getStatus();
        if ( status.getTime() % 20 == 0 ) {
            changeDirection();
        }
        
        stats.setCurrentStatus( status );
        
        // enemy not seen in 3 turns
        if ( enemy != null && getRadarTurnRemaining() == 0 && enemy.getTime() + 3 < getTime() ) {
            // no enemy in the radar
            enemy = null;
        }
    }

    private byte changeDirection() {
        if ( Math.random() > 0.5 ) {
            strafeDirection *= -1;
        }
        return strafeDirection;
    }

    /**
     * onScannedRobot: What to do when you see another robot
     */
    public void onScannedRobot( ScannedRobotEvent event ) {
        
        radarController.onScannedRobot( event );
        
        enemy = event;
        stats.add( event );
    }

    /**
     * Chechks the distance to the other robot once the gun has cooled down
     * 
     * @param robot
     * @return
     */
    private double getGunCooledDistance( ScannedRobotEvent robot ) {
        double timeToCool = getGunCoolingTime();
        Point2D futureEnemyLocation = getFutureEnemyLocation( robot, timeToCool );
        double deltaX = futureEnemyLocation.getX() - getX();
        double deltaY = futureEnemyLocation.getY() - getY();
        return Math.sqrt( deltaX * deltaX + deltaY * deltaY );
    }

    private void doMove() {
        if ( enemy == null ) {
            return;
        }
        
        if ( getGunCooledDistance( enemy ) > 200 ) {
            getCloser( enemy );
        }
        else if( getGunCooledDistance( enemy ) < 100 ) {
            fallBack( enemy );
        }
        else {
            strafeAround( enemy.getBearing() );
        }
    }

    private void getCloser( ScannedRobotEvent e ) {
        setTurnRight( e.getBearing() + 25 * strafeDirection );
        setAhead( getGunCooledDistance( e ) / 2.5 );
        if ( e.getVelocity() == 0 ) {
            safeFire( Rules.MAX_BULLET_POWER );
        }
    }
    
    private void fallBack( ScannedRobotEvent e ) {
        setTurnRight( e.getBearing() + 25 * strafeDirection );
        setAhead( getGunCooledDistance( e ) - 100 );
        if ( e.getVelocity() == 0 ) {
            safeFire( Rules.MAX_BULLET_POWER );
        }
    }
    
    @Override
    public void ahead( double distance ) {
        super.ahead( checkDistanceToWall( distance ) );
    }

    @Override
    public void setAhead( double distance ) {
        super.setAhead( checkDistanceToWall( distance ) );
    }
    
    private double checkDistanceToWall( double distance ) {
        return checkDistanceToWall( distance, distance, 0 );
    }

    private double checkDistanceToWall( double original, double distance, int iteration ) {
        Point2D futureLocation = EuclidUtils.getFutureLocation( getLocation(), EuclidUtils.toCarthesianDegrees( getHeading() ), distance );
        if ( !battleField.contains( futureLocation ) ) {
            if ( iteration > 10 ) {
                return - original / 2;
            }
            return checkDistanceToWall( original, distance * 0.9, iteration++ );
        }
        return distance;
    }
    
    private void strafeAround( double bearing ) {
        double degrees = bearing + 90;
        if ( Math.abs( degrees ) > 180 ) {
            degrees = ( degrees > 0 ) ? degrees - 360 : degrees + 360;
        }
        setTurnRight( degrees );
        setAhead( ( 100 + Math.random() * 50 ) * strafeDirection );
    }

    private void doAim() {
        
        if ( enemy == null ) {
            return;
        }
        
        final ScannedRobotEvent enemy = this.enemy;
        
        power = Math.min( enemy.getEnergy() / 4, 300 / enemy.getDistance() );
        power = Math.min( power, Rules.MAX_BULLET_POWER );
        power = Math.max( power, Rules.MIN_BULLET_POWER );
        
        // 1. try to find the time to aim + bullet fly time to target current location
        double bulletVelcity = 20 - 3 * power;
        double bulletTime = roundUp( enemy.getDistance() / bulletVelcity );
        double timeToTurn = roundUp( Math.abs( enemy.getBearing() / Rules.GUN_TURN_RATE ) );
        double time = bulletTime + Math.max( timeToTurn, getGunCoolingTime() );
        // 2. try to find the tank location & angle after that time ( currently
        // not taking into account, that if the enemy comes closer - time should
        // be smaller & other way around )
        double angle = getGunTurnAngleToShoot( enemy, time );
        
        // get more precise time
        while ( angle / Rules.GUN_TURN_RATE > timeToTurn ) {
            angle = getGunTurnAngleToShoot( enemy, angle / Rules.GUN_TURN_RATE );
            timeToTurn = angle / Rules.GUN_TURN_RATE;
        }
        
        setTurnGunRight( angle );
        
        fireAt = enemy;
        
        isAiming = true;
    }

    private double roundUp( double ttt ) {
        return BigDecimal.valueOf( ttt ).setScale( 0, RoundingMode.UP ).doubleValue();
    }

    private double getGunCoolingTime() {
        return getGunHeat() / getGunCoolingRate();
    }

    private boolean safeFire( double power ) {
        if ( power > 2 ) {
            setBulletColor( Color.red );
        } else if ( power > 1 ) {
            setBulletColor( Color.orange );
        } else if ( power > 0.5 ) {
            setBulletColor( Color.yellow );
        } else {
            setBulletColor( Color.white );
        }
        
        if ( getGunHeat() == 0 ) {
            // power must be in range of the rules
            power = Math.min( power, Rules.MAX_BULLET_POWER );
            power = Math.max( power, Rules.MIN_BULLET_POWER );
            setFire( power );
            
            return true;
        }
        
        return false;
    }

    private double getGunTurnAngleToShoot( ScannedRobotEvent e, double time ) {
        double angle = getFutureEnemyAngle( e, time ) - getGunHeading();
        
        // don't turn the gun more than 180 degrees to one direction, but change the sign
        if ( Math.abs( angle ) > 180 ) {
            angle = ( angle > 0 ) ? angle - 360 : angle + 360;
        }
        return angle;
    }

    @Override
    public void onRobotDeath( RobotDeathEvent event ) {
        if ( enemy.getName().equals( event.getName() ) ) {
            enemy = null;
            fireAt = null;
        }
    }
    
    @Override
    public void onBulletHit( BulletHitEvent event ) {
        // TODO Auto-generated method stub
        if ( lasthitRate < 3 ) {
            lasthitRate++;
        }
    }
    
    @Override
    public void onBulletHitBullet( BulletHitBulletEvent event ) {
        // TODO Auto-generated method stub
    }
    
    @Override
    public void onBulletMissed( BulletMissedEvent event ) {
        // TODO Auto-generated method stub
        if ( lasthitRate > 0 ) {
            lasthitRate--;
        }
    }
    
    /**
     * onHitByBullet: What to do when you're hit by a bullet
     */
    public void onHitByBullet( HitByBulletEvent e ) {
        strafeAround( e.getBearing() );
    }

    /**
     * onHitWall: What to do when you hit a wall
     */
    public void onHitWall( HitWallEvent e ) {
        changeDirection();
    }
    
    @Override
    public void onHitRobot( HitRobotEvent event ) {
        changeDirection();
    }
    
    @Override
    public void onWin( WinEvent event ) {
        setBodyColor( Color.red );
        setAhead( Double.POSITIVE_INFINITY );
        setTurnRight( Double.POSITIVE_INFINITY );
        setTurnGunLeft( Double.POSITIVE_INFINITY );
        execute();
    }
    
    @Override
    public void onCustomEvent( CustomEvent event ) {
        if ( "velocityChanged".equals( event.getCondition().name ) ) {
            onVelocityChanged( event );
        }
    }

    protected void onVelocityChanged( Event event ) {
        isAiming = false;
    }
    
    @Override
    public void onPaint( Graphics2D g ) {
        if ( fireAt == null ) {
            return;
        }
        
        Point2D l = getFutureEnemyLocation( fireAt, 0 );
        
        // Set the paint color to a red half transparent color
        g.setColor(new Color(0xff, 0x00, 0x00, 0x80));
    
        // Draw a line from our robot to the scanned robot
        g.drawLine((int)l.getX(), (int)l.getY(), (int)getX(), (int)getY());
    
        // Draw a filled square on top of the scanned robot that covers it
        g.fillRect((int)l.getX() - 20, (int)l.getY() - 20, 40, 40);
        
        g.setColor( Color.red );
        g.drawOval( ( int ) l.getX() - 18, ( int ) l.getY() - 18, 36, 36 );
    }
    
}