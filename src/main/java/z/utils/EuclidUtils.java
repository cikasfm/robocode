package z.utils;

import java.awt.geom.Point2D;

/**
 * @author Zilvinas Vilutis
 *
 */
public abstract class EuclidUtils {

    /**
     * Predicting future location based on current 
     * 
     * @param time
     * @return
     */
    public static Point2D getFutureLocation( Point2D currentLocation, double direction, double time, double velocity ) {
        double distance = time * velocity;
        return getFutureLocation( currentLocation, direction, distance );
    }

    /**
     * @param currentLocation
     * @param direction in Carthesian Degrees
     * @param distance
     * @return
     */
    public static Point2D getFutureLocation( Point2D currentLocation, double direction, double distance ) {
        double directionInRadians = Math.toRadians( direction );
        double newX = currentLocation.getX() + distance * Math.cos( directionInRadians );
        double newY = currentLocation.getY() + distance * Math.sin( directionInRadians );
        return new Point2D.Double( newX, newY );
    }

    /**
     * This method would transform degrees from Carthesian Coordinate System to
     * Euclidean Coordinate System
     * 
     * Returns a normalized value between 0 and 360
     * 
     * @param cartesianDegrees
     * @return
     */
    public static double toEuclidDegrees( double cartesianDegrees ) {
        return normalize( 90 - cartesianDegrees );
    }
    
    /**
     * Transforms Euclidean Degrees to Carthesian value
     * 
     * Returns a normalized value between 0 and 360
     * 
     * @param euclidDegrees
     * @return
     */
    public static double toCarthesianDegrees( double euclidDegrees ) {
        return normalize( 90 - euclidDegrees );
    }

    /**
     * Makes sure that the number is between 0 and 360 by adding or subtracting
     * 360 degrees
     * 
     * @param degrees
     * @return
     */
    public static double normalize( double degrees ) {
        while ( degrees > 360 ) {
            degrees -= 360;
        }
        while ( degrees < 0 ) {
            degrees += 360;
        }
        return degrees;
    }
    
}
