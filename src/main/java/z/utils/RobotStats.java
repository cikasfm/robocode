package z.utils;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;

import robocode.*;

/**
 * Gathers statistics of robot movements. May be used for movement prediction as in MogBot
 * 
 * @author Zilvinas Vilutis
 * @since 1.0
 */
public class RobotStats {
    
    private static final int MAX_EVENTS = 500;
    
    private RobotStatus currentStatus = null;
    
    private RobotStatus previousStatus = null;
    
    private final Map<String, Queue<ScannedRobotEvent>> robotEvents = new HashMap<String, Queue<ScannedRobotEvent>>();
    
    public void add( ScannedRobotEvent e ) {
        if ( !robotEvents.containsKey( e.getName() ) ) {
            robotEvents.put( e.getName(), new ArrayBlockingQueue<ScannedRobotEvent>( MAX_EVENTS ) );
        }
        Queue<ScannedRobotEvent> queue = robotEvents.get( e.getName() );
        if ( queue.size() >= MAX_EVENTS ) {
            queue.poll();
        }
        queue.add( e );
    }
    
    public Iterator<ScannedRobotEvent> getStats( String robotName ) {
        if ( !robotEvents.containsKey( robotName ) ) {
            throw new IllegalArgumentException( "Robot with name '" + robotName + "' has no stats recorded!" );
        }
        Queue<ScannedRobotEvent> queue = robotEvents.get( robotName );
        return queue.iterator();
    }
    
    public boolean hasStats( String robotName ) {
        return robotEvents.containsKey( robotName );
    }

    public RobotStatus getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus( RobotStatus status ) {
        this.previousStatus = this.currentStatus;
        this.currentStatus = status;
    }
    
    public RobotStatus getPreviousStatus() {
        return previousStatus;
    }

}
