package z.robot;

import static org.junit.Assert.*;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import robocode.exception.RobotException;
import z.utils.EuclidUtils;

@RunWith( JUnit4.class )
public class AbstractZRobotTest {

    private AbstractZRobot euclideanRobot;

    @Before
    public void before() {
        euclideanRobot = new AbstractZRobot(){};
        euclideanRobot.run();
    }

    @Test( expected = RobotException.class )
    public void testGetCoords() {
        euclideanRobot.getLocation();
    }

    @Test
    public void testEuclidUtilsToEuclidDegrees() {
        assertEquals( 0, EuclidUtils.toEuclidDegrees( 90 ), 0 );
        assertEquals( 45, EuclidUtils.toEuclidDegrees( 45 ), 0 );
        assertEquals( 90, EuclidUtils.toEuclidDegrees( 0 ), 0 );
        assertEquals( 270, EuclidUtils.toEuclidDegrees( 180 ), 0 );
        assertEquals( 180, EuclidUtils.toEuclidDegrees( 270 ), 0 );
    }

    @Test
    public void testEuclidUtilsToCarthesianDegrees() {
        assertEquals( 90, EuclidUtils.toCarthesianDegrees( 0 ), 0 );
        assertEquals( 45, EuclidUtils.toCarthesianDegrees( 45 ), 0 );
        assertEquals( 0, EuclidUtils.toCarthesianDegrees( 90 ), 0 );
        assertEquals( 270, EuclidUtils.toCarthesianDegrees( 180 ), 0 );
        assertEquals( 180, EuclidUtils.toCarthesianDegrees( 270 ), 0 );
    }

}
